var SnakeBody;

var SnakeCurve = (function () {
  SnakeCurve.prototype = new SnakeBody();
  SnakeCurve.prototype.constructor = SnakeCurve;

  function SnakeCurve(x, y, directionTo) {
    SnakeBody.call(this, x, y, directionTo);
  }

  SnakeCurve.prototype.getGraphic = function () {
    return 'rgba(0, 0, 255, 1)';
  };

  return SnakeCurve;
}());
