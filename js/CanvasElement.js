var CanvasElement = (function () {
  function CanvasElement(x, y) {
    this.x = x;
    this.y = y;
  }

  CanvasElement.prototype.getX = function () {
    return this.x;
  };

  CanvasElement.prototype.getY = function () {
    return this.y;
  };

  CanvasElement.prototype.colides = function (element) {
    return this.x === element.getX() && this.y === element.getY();
  };

  return CanvasElement;
}());
