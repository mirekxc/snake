var SnakeBody;

var SnakeHead = (function () {
  SnakeHead.prototype = new SnakeBody();
  SnakeHead.prototype.constructor = SnakeHead;

  function SnakeHead(x, y, directionTo) {
    SnakeBody.call(this, x, y, directionTo);
  }

  SnakeHead.prototype.getGraphic = function () {
    return 'rgba(0, 255, 0, 1)';
  };

  return SnakeHead;
}());
