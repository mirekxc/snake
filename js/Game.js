var Direction;
var Event;
var Snake;
var SnakeFood;

var Game = (function () {
  function Game(gameArea, ticker) {
    this.ticker = ticker;
    this.gameArea = gameArea;

    this.ticker.on(Event.TICK, this.tick.bind(this));
    this.ticker.on(Event.TICKER_STOP, this.pause.bind(this));
    this.ticker.on(Event.TICKER_START, this.resume.bind(this));

    this.resetGame();
  }

  Game.prototype.resetGame = function () {
    this.ticker.setInterval(100);

    this.maxSnakeFood = 3;
    this.initialSnakeLength = 3;

    this.direction = Direction.NORTH;
    this.newDirection = this.direction;
    this.queuedDirection = null;
    this.paused = false;
    this.gameIsOver = false;
    this.snakeFood = [];

    this.snake = new Snake(
      Math.floor(this.gameArea.getWidth()/2),
      Math.floor(this.gameArea.getHeight()/2),
      this.direction,
      this.initialSnakeLength
    );

    this.snake.on(Event.SNAKE_COLLISION, this.gameOver.bind(this));

    this.throwFood();

    this.gameArea.redraw(this.snake.getParts(), this.snakeFood);
  };

  Game.prototype.setDirection = function (direction) {
    if (this.paused) {
      return;
    }

    if (this.direction === this.newDirection) {
      if (!Direction.areOppositeDirections(this.direction, direction)) {
        this.newDirection = direction;
      }

      return;
    }

    if (!Direction.areOppositeDirections(this.newDirection, direction)) {
      this.queuedDirection = direction;
    }
  };

  Game.prototype.pause = function () {
    this.paused = true;
  };

  Game.prototype.resume = function () {
    if (this.gameIsOver) {
      this.resetGame();
      return;
    }

    this.paused = false;
  };

  Game.prototype.throwFood = function () {
    var snakeParts = this.snake.getParts();

    if (
      this.snakeFood.length >= this.maxSnakeFood ||
      snakeParts.length + this.snakeFood.length >= this.gameArea.getWidth() * this.gameArea.getHeight()
    ) {
      return;
    }

    var food;

    do {
      var collision = false;
      food = new SnakeFood(
        Math.floor(Math.random() * this.gameArea.getWidth()),
        Math.floor(Math.random() * this.gameArea.getHeight())
      );

      for (var i = 0; i < snakeParts.length; i++) {
        var part = snakeParts[i];

        if (part.colides(food)) {
          collision = true;
          break;
        }
      }

    } while (collision);

    this.snakeFood.push(food);

    if (this.snakeFood.length < this.maxSnakeFood) {
      this.throwFood();
    }
  };

  Game.prototype.tick = function () {
    this.direction = this.newDirection;

    if (null !== this.queuedDirection) {
      this.newDirection = this.queuedDirection;
    }
    this.queuedDirection = null;

    var snakeHead = this.snake.getHead();
    var newX = snakeHead.x;
    var newY = snakeHead.y;

    switch (this.direction) {
      case Direction.WEST:
        newX--;
        break;
      case Direction.NORTH:
        newY--;
        break;
      case Direction.EAST:
        newX++;
        break;
      case Direction.SOUTH:
        newY++;
        break;
      default:
    }

    this.snake.addPart(
      this.gameArea.normalizeX(newX),
      this.gameArea.normalizeY(newY),
      this.direction
    );

    if (this.snakeEatsFood()) {
      this.snake.grow();

      if (0 === (this.snake.getParts().length - this.initialSnakeLength + 1)%10) {
        var timeout = this.ticker.getInterval();
        this.ticker.setInterval(timeout - 5);
      }
    }

    this.gameArea.redraw(this.snake.getParts(), this.snakeFood);
  };

  Game.prototype.snakeEatsFood = function () {
    var snakeHead = this.snake.getHead();

    for (var i = 0; i < this.snakeFood.length; i++) {
      var food = this.snakeFood[i];

      if (food.colides(snakeHead)) {
        this.snakeFood.splice(i, 1);
        this.throwFood();

        return true;
      }
    }

    return false;
  };

  Game.prototype.gameOver = function () {
    this.gameIsOver = true;
    this.ticker.stop();
  };

  return Game;
}());
