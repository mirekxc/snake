EventDispatcher = (function () {
  function EventDispatcher() {
    this.events = {};
  }

  EventDispatcher.prototype.on = function (event, callback) {
    this.events[event] = this.events[event] || [];

    if (this.events[event]) {
      this.events[event].push(callback);
    }
  };

  EventDispatcher.prototype.off = function (event, callback) {
    if (this.events.hasOwnProperty(event)) {
      if (undefined === callback) {
        delete this.events[event];
        return true;
      }

      var listeners = this.events[event];
      var callbackIndex = listeners.indexOf(callback);

      if (callbackIndex > -1) {
        listeners.splice(callbackIndex, 1);
        return true;
      }

    }

    return false;
  };

  EventDispatcher.prototype.emit = function (event) {
    if (this.events.hasOwnProperty(event.type)) {
      var listeners = this.events[event.type];
      var listenersCount = listeners.length;

      while (listenersCount--) {
        listeners[listenersCount].call(this, event);
      }
    }
  };

  EventDispatcher.prototype.clearEvents = function () {
    this.events = {};
  };

  return EventDispatcher;
})();