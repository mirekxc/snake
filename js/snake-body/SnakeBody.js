var CanvasElement;

var SnakeBody = (function () {
  SnakeBody.prototype = new CanvasElement();
  SnakeBody.prototype.constructor = SnakeBody;

  function SnakeBody(x, y, directionTo) {
    this.directionTo = directionTo;
    this.directionFrom = directionTo;

    CanvasElement.call(this, x, y);
  }

  SnakeBody.prototype.getDirectionTo = function () {
    return this.directionTo;
  };

  SnakeBody.prototype.setDirectionFrom = function (direction) {
    this.directionFrom = direction;
  };

  SnakeBody.prototype.getDirectionFrom = function () {
    return this.directionFrom;
  };

  return SnakeBody;
}());
