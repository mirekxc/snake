var Direction;
var EventDispatcher;
var Event;

var Snake = (function () {
  Snake.prototype = new EventDispatcher();
  Snake.prototype.constructor = Snake;

  function Snake(initialX, initialY, initialDirection, initialLength) {
    EventDispatcher.call(this);

    this.parts = [];
    this.length = 3;

    if (undefined === initialX) {
      initialX = 0;
    }

    if (undefined === initialY) {
      initialY = 0;
    }

    if (undefined !== initialLength) {
      this.length = initialLength;
    }

    this.addPart(initialX, initialY, initialDirection);
  }

  Snake.prototype.grow = function () {
    this.length++;
  };

  Snake.prototype.getHead = function () {
    return this.parts[0];
  };

  Snake.prototype.getParts = function () {
    return this.parts;
  };

  Snake.prototype.addPart = function (x, y, direction) {
    var newHead = new SnakeHead(x, y, direction);

    if (this.parts.length) {
      var head = this.parts.shift();
      this.parts.unshift(this.headToBody(head, direction));
    }

    this.parts.unshift(newHead);

    if (this.parts.length > this.length) {
      this.parts.pop();
    }

    if (this.parts.length > 1) {
      var lastBody = this.parts.pop();
      this.parts.push(this.bodyToTail(lastBody));
    }

    for (var i=1; i<this.parts.length; i++) {
      var part = this.parts[i];

      if (newHead.colides(part)) {
        this.emit(new Event(Event.SNAKE_COLLISION));
      }
    }
  };

  Snake.prototype.headToBody = function (head, newDirection) {
    var x = head.getX();
    var y = head.getY();
    var directionTo = head.getDirectionTo();

    var snakeBody = new SnakeBody(x, y, newDirection);

    if (newDirection !== directionTo) {
      snakeBody = new SnakeCurve(x, y, newDirection);
      snakeBody.setDirectionFrom(directionTo);
    }

    return snakeBody;
  };

  Snake.prototype.bodyToTail = function (body) {
    var x = body.getX();
    var y = body.getY();
    var directionTo = body.getDirectionTo();

    var snakeTail = new SnakeTail(x, y, directionTo);

    return snakeTail;
  };

  return Snake;
}());
