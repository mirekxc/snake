var Direction;

(function (Direction) {
  Direction.NORTH = 'N';
  Direction.SOUTH = 'S';
  Direction.EAST = 'E';
  Direction.WEST = 'W';

  function areOpposite(direction1, direction2) {
    return (
      (direction1 === Direction.EAST && direction2 === Direction.WEST) ||
      (direction1 === Direction.WEST && direction2 === Direction.EAST) ||
      (direction1 === Direction.NORTH && direction2 === Direction.SOUTH) ||
      (direction1 === Direction.SOUTH && direction2 === Direction.NORTH)
    );
  }

  Direction.areOppositeDirections = areOpposite;
})(Direction || (Direction = {}));
