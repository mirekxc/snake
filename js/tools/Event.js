var Event = (function () {
  Event.TICK = 'event.ticker.tick';
  Event.TICKER_STOP = 'event.ticker.stop';
  Event.TICKER_START = 'event.ticker.start';
  Event.AREA_LOADED = 'event.area.loaded';
  Event.SNAKE_COLLISION = 'event.snake.collision';

  function Event(type, data) {
    this.type = type;
    this.data = data;
  }

  return Event;
}());
