var SnakeBody;

var SnakeTail = (function () {
  SnakeTail.prototype = new SnakeBody();
  SnakeTail.prototype.constructor = SnakeTail;

  function SnakeTail(x, y, directionTo) {
    SnakeBody.call(this, x, y, directionTo);
  }

  SnakeTail.prototype.getGraphic = function () {
    return 'rgba(0, 0, 0, 1)';
  };

  return SnakeTail;
}());
