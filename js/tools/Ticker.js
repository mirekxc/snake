var Event;
var EventDispatcher;

var Ticker = (function () {
  Ticker.prototype = new EventDispatcher();
  Ticker.prototype.constructor = Ticker;

  function Ticker() {
    EventDispatcher.call(this);

    this.intervalTimeout = 1000;
    this.intervalHandler;
  }

  Ticker.prototype.setInterval = function (timeout) {
    var inProgress = !!this.intervalHandler;

    this.stop();
    this.intervalTimeout = timeout;

    if (inProgress) {
      this.start();
    }
  };

  Ticker.prototype.getInterval = function () {
    return this.intervalTimeout;
  };

  Ticker.prototype.toggle = function () {
    if (!!this.intervalHandler) {
      this.stop();
      return;
    }

    this.start();
  };

  Ticker.prototype.stop = function () {
    if (!!this.intervalHandler) {
      this.emit(new Event(Event.TICKER_STOP));
      clearInterval(this.intervalHandler);
      this.intervalHandler = null;
    }
  };

  Ticker.prototype.start = function () {
    if (!this.intervalHandler) {
      this.emit(new Event(Event.TICKER_START));
      this.intervalHandler = setInterval(this.tick.bind(this), this.intervalTimeout);
    }
  };

  Ticker.prototype.tick = function () {
    this.emit(new Event(Event.TICK));
  };

  return Ticker;
}());
