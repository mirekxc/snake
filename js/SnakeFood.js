var CanvasElement;

var SnakeFood = (function () {
  SnakeFood.prototype = new CanvasElement();
  SnakeFood.prototype.constructor = SnakeFood;

  function SnakeFood(x, y) {
    CanvasElement.call(this, x, y);
  }

  return SnakeFood;
}());
