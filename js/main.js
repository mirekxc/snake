var Ticker;
var Event;
var GameArea;
var Game;
var Direction;

(function load() {
  function initGame(gameArea) {

    var ticker = new Ticker();

    var game = new Game(gameArea, ticker);

    window.addEventListener('keydown', function (e) {
      switch (e.keyCode) {
        case 37:
          game.setDirection(Direction.WEST);
          break;
        case 38:
          game.setDirection(Direction.NORTH);
          break;
        case 39:
          game.setDirection(Direction.EAST);
          break;
        case 40:
          game.setDirection(Direction.SOUTH);
          break;
        case 32:
          ticker.toggle();
          break;
        default:
      }
    });

    ticker.start();
  }

  var ga = new GameArea('game-area', 21, 21, 30);
  ga.on(Event.AREA_LOADED, initGame.bind(this, ga));

})();
