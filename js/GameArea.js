var EventDispatcher;
var Event;
var SnakeHead;
var SnakeBody;
var SnakeCurve;
var SnakeTail;
var Direction;

var GameArea = (function () {
  GameArea.prototype = new EventDispatcher();
  GameArea.prototype.constructor = GameArea;

  function GameArea(containerId, width, height, size) {
    EventDispatcher.call(this);
    var self = this;

    this.width = width || 11;
    this.height = height || 11;
    this.size = size || 30;
    this.loaded = false;
    this.TO_RADIANS = Math.PI/180;

    this.container = document.getElementById(containerId);
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');

    this.sprite = new Image();
    this.sprite.onload = function () {
      self.emit(new Event(Event.AREA_LOADED));
      self.loaded = true;
    };
    this.sprite.src = 'src/img/snake.png';

    this.container.appendChild(this.canvas);
  }

  GameArea.prototype.on = function (event, callback) {
    EventDispatcher.prototype.on.call(this, event, callback);

    if (Event.AREA_LOADED === event && this.loaded) {
      callback.call(this);
    }
  };

  GameArea.prototype.getWidth = function () {
    return this.width;
  };

  GameArea.prototype.getHeight = function () {
    return this.height;
  };

  GameArea.prototype.normalizeX = function (x) {
    x = x%this.width;

    if (x < 0) {
      x += this.width;
    }

    return x;
  };

  GameArea.prototype.normalizeY = function (y) {
    y = y%this.height;

    if (y < 0) {
      y += this.height;
    }

    return y;
  };

  GameArea.prototype.redraw = function (snakeParts, food) {
    this.clear();
    this.draw(snakeParts, food);
  };

  GameArea.prototype.clear = function () {
    this.canvas.width = this.width * this.size;
    this.canvas.height = this.height * this.size;
  };

  GameArea.prototype.draw = function (snakeParts, food) {
    var i;

    for (i = food.length - 1; i >= 0; i--) {
      this.drawElement(food[i]);
    }

    for (i = snakeParts.length - 1; i >= 0; i--) {
      this.drawElement(snakeParts[i]);
    }

    this.ctx.font = '30px Verdana';
    this.ctx.fillText(snakeParts.length, 10, 40);
    this.ctx.strokeStyle = 'rgba(255, 255, 255, 1)';
    this.ctx.strokeText(snakeParts.length, 10, 40);

  };

  GameArea.prototype.drawElement = function (element) {
    var rotation = 0;
    var spriteX = 0;
    var spriteY = 0;
    var spriteSize = 64;

    if (element.constructor ===  SnakeHead) {
      spriteX = 3;
      rotation = this.getHeadRotation(element.getDirectionTo());
    }

    else if (element.constructor ===  SnakeBody) {
      spriteX = 1;
      rotation = this.getBodyRotation(element.getDirectionTo());
    }

    else if (element.constructor ===  SnakeCurve) {
      spriteX = 0;
      rotation = this.getCurveRotation(element.getDirectionFrom(), element.getDirectionTo());
    }

    else if (element.constructor ===  SnakeTail) {
      spriteX = 2;
      rotation = this.getTailRotation(element.getDirectionTo());
    }

    else if (element.constructor ===  SnakeFood) {
      spriteX = 4;
    }

    this.ctx.save();
    this.ctx.translate(element.x * this.size + this.size/2, element.y * this.size + this.size/2);
    this.ctx.rotate(rotation * this.TO_RADIANS);

    this.ctx.drawImage(
      this.sprite,
      spriteX * spriteSize,
      spriteY * spriteSize,
      spriteSize,
      spriteSize,
      -this.size/2,
      -this.size/2,
      this.size,
      this.size
    );

    this.ctx.restore();
  };

  GameArea.prototype.getCurveRotation = function (directionFrom, directionTo) {
    if (
      (Direction.EAST === directionFrom && Direction.SOUTH === directionTo) ||
      (Direction.NORTH === directionFrom && Direction.WEST === directionTo)
    ) {
      return 90;
    }

    if (
      (Direction.SOUTH === directionFrom && Direction.EAST === directionTo) ||
      (Direction.WEST === directionFrom && Direction.NORTH === directionTo)
    ) {
      return -90;
    }

    if (
      (Direction.SOUTH === directionFrom && Direction.WEST === directionTo) ||
      (Direction.EAST === directionFrom && Direction.NORTH === directionTo)
    ) {
      return 180;
    }

    return 0;
  };

  GameArea.prototype.getBodyRotation = function (direction) {
    if (
      Direction.EAST === direction ||
      Direction.WEST === direction
    ) {
      return 90;
    }

    return 0;
  };

  GameArea.prototype.getHeadRotation = function (direction) {
    if (Direction.EAST === direction) {
      return 90;
    }

    if (Direction.WEST === direction) {
      return -90;
    }

    if (Direction.SOUTH === direction) {
      return 180;
    }

    return 0;
  };

  GameArea.prototype.getTailRotation = function (direction) {
    if (Direction.EAST === direction) {
      return 90;
    }

    if (Direction.WEST === direction) {
      return -90;
    }

    if (Direction.SOUTH === direction) {
      return 180;
    }

    return 0;
  };

  return GameArea;
}());
